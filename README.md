# IRPF Automate

Ferramenta para auxiliar no cálculo do custo de ações, FIIs, ETFs para a declaração de Bens e Direitos do Imposto de Renda Pessoa Física (IRPF).

## Disclaimer
No momento é apenas um jupyter notebook 😛

## Como usar:
1. Fazer login na [Área do Investidor](https://www.investidor.b3.com.br/area-do-investidor) da B3,
2. Para cada ano:
    1. Entrar no menu Extratos → Negociação → Filtrar entre as datas 01 de Janeiro e 31 de Dezembro para cada ano
    2. Clicar no botão de Exportar para Excel

Por fim, basta executar o Jupyter Notebook `notebooks/IRPF_Automate`, usando essas planilhas exportadas anteriormente.

## TODO:
- [ ] Scrap do nome e CNPJ de um dado BDR, possivelmente no site https://statusinvest.com.br/bdrs/
- [ ] Criar CLI, ao invés de usar o notebook


## Colaboração
PRs sempre bem vindos!
