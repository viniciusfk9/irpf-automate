"""
author: Lauro de Lacerda
e-mail: laurodelacerda@gmail.com

Tests for the module irpf
"""

import pytest
import irpf


########################################################################################################################


tests_bdr = [
    ("AAPL34", {"Nome": "APPLE INC.",         "Tipo": "BDR"}),
    ("TSLA34", {"Nome": "TESLA. INC",         "Tipo": "BDR"}),
    ("GOGL34", {"Nome": "ALPHABET INC.",      "Tipo": "BDR"}),
    ("FBOK34", {"Nome": "META PLATFORMS INC", "Tipo": "BDR"}),
]

@pytest.mark.parametrize(["ticker", "result"], tests_bdr)
def test_get_bdr_info(ticker, result):
    assert irpf.get_bdr_info(ticker) == result


########################################################################################################################


tests_stock = [
    ("ABEV3F", {"CNPJ": "07.526.557/0001-00", "Nome": "AMBEV S.A.",          "Tipo": "Ação"}),
    ("AZUL4F", {"CNPJ": "09.305.994/0001-29", "Nome": "AZUL S.A.",           "Tipo": "Ação"}),
    ("MGLU3",  {"CNPJ": "47.960.950/0001-21", "Nome": "MAGAZINE LUIZA S.A.", "Tipo": "Ação"}),
    ("MGLU3F", {"CNPJ": "47.960.950/0001-21", "Nome": "MAGAZINE LUIZA S.A.", "Tipo": "Ação"}),
    ("VALE3F", {"CNPJ": "33.592.510/0001-54", "Nome": "VALE S.A.",           "Tipo": "Ação"}),
]

@pytest.mark.parametrize(["ticker", "result"], tests_stock)
def test_get_stock_info(ticker, result):
    assert irpf.get_stock_info(ticker) == result


########################################################################################################################


tests_fii = [
    (
        "ALZR11",
         {
             "CNPJ":             "28.737.771/0001-85",
             "Nome":             "Alianza Trust Renda Imobiliária",
             "Prazo de duração": "Indeterminado",
             "Segmento ANBIMA":  "Híbrido",
             "Tipo":             "FII",
             "Tipo ANBIMA":      "Renda",
             "Ínicio do fundo":  "27/12/2017"
         }
    ),
    (
        "BCFF11",
        {
            "CNPJ":             "11.026.627/0001-38",
            "Nome":             "BTG Fundo de Fundos",
            "Prazo de duração": "Indeterminado",
            "Segmento ANBIMA":  "Títulos e Valores Mobiliários",
            "Tipo":             "FII",
            "Tipo ANBIMA":      "Títulos e Valores Mobiliários",
            "Ínicio do fundo":  "07/01/2010"
        }
    ),
    (
        "IRDM11",
        {
            "CNPJ":             "28.830.325/0001-10",
            "Nome":             "Iridium Recebíveis Imobiliários",
            "Prazo de duração": "Indeterminado",
            "Segmento ANBIMA":  "Títulos e Valores Mobiliários",
            "Tipo":             "FII",
            "Tipo ANBIMA":      "Títulos e Valores Mobiliários",
            "Ínicio do fundo":  "06/03/2018"
        }
    ),
    (
        "MALL11",
        {
            "CNPJ":             "26.499.833/0001-32",
            "Nome":             "Malls Brasil Plural",
            "Prazo de duração": "Indeterminado",
            "Segmento ANBIMA":  "Shoppings",
            "Tipo":             "FII",
            "Tipo ANBIMA":      "Renda",
            "Ínicio do fundo":  "14/12/2017"
        }
    ),
]

@pytest.mark.parametrize(["ticker", "result"], tests_fii)
def test_get_fii_info(ticker, result):
    assert irpf.get_fii_info(ticker) == result


########################################################################################################################


tests_etf = [
    (
        "XINA11",
        {
            "BookName":                      "TREND CHINA",
            "CNPJ":                          "38.542.870/0001-65",
            "Código ISIN":                   "BRXINACTF006",
            "Data de início":                "18/12/2020",
            "Lote Mínimo (Emissão/Resgate)": "--",
            "Lote Padrão":                   "1",
            "Mercado":                       "--",
            "Nome":                          "TREND ETF MSCI CHINA",
            "Nº de cotistas":                "45.815",
            "Ratio":                         "-",
            "Tipo":                          "ETF"
        }
    ),
    (
        "BOVA11",
        {
            "BookName":                      "ISHARES BOVA",
            "CNPJ":                          "10.406.511/0001-61",
            "Código ISIN":                   "BRBOVACTF003",
            "Data de início":                "19/11/2008",
            "Lote Mínimo (Emissão/Resgate)": "50.000",
            "Lote Padrão":                   "1",
            "Mercado":                       "A Vista e Fracionário",
            "Nome":                          "ISHARES IBOVESPA FUNDO DE ÍNDICE",
            "Nº de cotistas":                "121.246",
            "Ratio":                         "1.000",
            "Tipo":                          "ETF"
        }
    ),
    (
        "BRAX11",
        {
            "BookName":                      "ISHARES BRAX",
            "CNPJ":                          "11.455.378/0001-04",
            "Código ISIN":                   "BRBRAXCTF002",
            "Data de início":                "23/12/2009",
            "Lote Mínimo (Emissão/Resgate)": "100.000",
            "Lote Padrão":                   "1",
            "Mercado":                       "A Vista e Fracionário",
            "Nome":                          "iShares IBrX - Índice Brasil (IBrX-100) Fundo de Índice",
            "Nº de cotistas":                "2.907",
            "Ratio":                         "500",
            "Tipo":                          "ETF"
        }
    )
]

@pytest.mark.parametrize(["ticker", "result"], tests_etf)
def test_get_etf_info(ticker, result):
    assert irpf.get_etf_info(ticker) == result


########################################################################################################################


tests_ticker = [
    ("AAPL34", {"CNPJ": "",                   "Nome": "APPLE INC.",                                              "Tipo": "BDR"}),
    ("AZUL4F", {"CNPJ": "09.305.994/0001-29", "Nome": "AZUL S.A.",                                               "Tipo": "Ação"}),
    ("IRDM11", {"CNPJ": "28.830.325/0001-10", "Nome": "Iridium Recebíveis Imobiliários",                         "Tipo": "FII"}),
    ("BRAX11", {"CNPJ": "11.455.378/0001-04", "Nome": "iShares IBrX - Índice Brasil (IBrX-100) Fundo de Índice", "Tipo": "ETF"} ),
]

@pytest.mark.parametrize(["ticker", "result"], tests_ticker)
def test_get_ticker_info(ticker, result):
    assert irpf.get_ticker_info(ticker) == result


########################################################################################################################

